#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
Programa cliente UDP que abre un socket a un servidor
"""

import socket
import sys

# Dirección IP y puerto del servidor.
SERVER = 'localhost'
PORT = 6001


def main():
    # Creamos el socket, lo configuramos y lo atamos a un servidor/puerto
    try:
        METHOD = sys.argv[1]
        RECEPTOR = sys.argv[2]
        lista1 = RECEPTOR.split('@')
        lista2 = lista1[1].split(':')
        RECEIVER = lista1[0]
        IP = lista2[0]
        SIPPORT = lista2[1]
    except IndexError or ValueError:
        sys.exit("Usage: python3 client.py <method> <receiver>@<IP>:<SIPport>")

    # Creamos el socket y lo configuramos
    try:
        with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as my_socket:
            my_socket.connect((SERVER, PORT))
            if METHOD == "INVITE":
                version = "0"
                origin = RECEIVER + "@" + RECEIVER + " " + IP
                session = "misesion"
                time = "0"
                media = "audio " + "7080 " + "RTP"
                content_type = "Content-Type: application/sdp"
                lenbytesbody = sys.getsizeof("v=" + version + '\r\n' + "o=" + origin + '\r\n' +
                                             "s=" + session + '\r\n' + "t=" + time + '\r\n' +
                                             "m=" + media + '\r\n')
                content_length = "Content-Length: " + str(lenbytesbody)
                my_socket.send(bytes(METHOD.upper() + ' sip:' + RECEIVER + '@' + IP + ' SIP/2.0' + '\r\n', 'utf-8') +
                               bytes(content_type + '\r\n', 'utf-8') +
                               bytes(content_length + '\r\n\r\n', 'utf-8') +
                               bytes('v=' + version + '\r\n', 'utf-8') +
                               bytes('o=' + origin + '\r\n', 'utf-8') +
                               bytes('s=' + session + '\r\n', 'utf-8') +
                               bytes('t=' + time + '\r\n', 'utf-8') +
                               bytes('m=' + media + '\r\n', 'utf-8') + b'\r\n\r\n')

            else:
                my_socket.send(bytes(METHOD.upper() + ' sip:' + RECEIVER + '@' + IP + ' SIP/2.0' + '\r\n\r\n', 'utf-8'))

            data = my_socket.recv(1024)
            print(data.decode('utf-8'))
        print("Cliente terminado.")
    except ConnectionRefusedError:
        print("Error conectando a servidor")


if __name__ == "__main__":
    main()