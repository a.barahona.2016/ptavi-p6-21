#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
Clase (y programa principal) para un servidor de eco en UDP simple
"""

import socketserver
import sys
import os
import simplertp
import random


# Puerto
PORT = 6001
audio_file = sys.argv[3]


class EchoHandler(socketserver.DatagramRequestHandler):
    """
    UDP echo handler class
    """
    dicc = {}

    def handle(self):
        msg = ""
        for line in self.rfile:
            msg = msg + line.decode('utf-8')
        IP = self.client_address[0]
        SERVERPORT = self.client_address[1]
        lista = msg.split('\r\n')
        cabecera = lista[0]
        lista1 = cabecera.split(" ")
        lista2 = lista1[1].strip('sip:')
        client = lista2.split("@")
        USERNAME = client[0]
        METHOD = lista1[0]
        if METHOD == "INVITE":
            if lista1[2] != "SIP/2.0" or not lista1[1].startswith("sip:"):
                self.wfile.write(b"SIP/2.0 400 Bad Request\r\n\r\n")
            else:
                version = lista[4]
                origin = lista[5]
                session = lista[6]
                time = lista[7]
                media = lista[8]
                content_type = lista[1]
                content_length = lista[2]
                self.wfile.write(b"SIP/2.0 200 OK\r\n" +
                                 bytes(content_type + '\r\n', 'utf-8') +
                                 bytes(content_length + '\r\n\r\n', 'utf-8') +
                                 bytes(version + '\r\n', 'utf-8') +
                                 bytes(origin + '\r\n', 'utf-8') +
                                 bytes(session + '\r\n', 'utf-8') +
                                 bytes(time + '\r\n', 'utf-8') +
                                 bytes(media + '\r\n', 'utf-8'))
        elif METHOD == "BYE":
            self.wfile.write(b"SIP/2.0 200 OK\r\n\r\n")
        elif METHOD == "ACK":
            RTP_header = simplertp.RtpHeader()
            RTP_header.set_header(pad_flag=0, ext_flag=0, cc=0, ssrc=random.randrange(100))
            audio = simplertp.RtpPayloadMp3(audio_file)
            simplertp.send_rtp_packet(RTP_header, audio, IP, 7080)
        elif METHOD != "INVITE" and "BYE" and "ACK":
            self.wfile.write(b"SIP/2.0 405 Method Not Allowed\r\n\r\n")


def main():
    try:
        ip = sys.argv[1]
        port = sys.argv[2]
        audio_file = sys.argv[3]
        if os.path.isfile("/Users/adrian/Desktop/URJC/3º/PTAVI/2/ptavi-p6-21/cancion.mp3"):
            pass
        else:
            print("No existe el fichero")
            exit()
    except IndexError:
        sys.exit("Usage: python3 server.py <IP> <port> <audio_file>")

    try:
        serv = socketserver.UDPServer(('', PORT), EchoHandler)
        print("Listening...")
    except OSError as e:
        sys.exit(f"Error empezando a escuchar: {e.args[1]}.")

    try:
        serv.serve_forever()
    except KeyboardInterrupt:
        print("Finalizado servidor")
        sys.exit(0)


if __name__ == "__main__":
    main()

